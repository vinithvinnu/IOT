
int brightness = 1;    // how bright the LED is
int fadeAmount = 7;
void setup() {
         
 pinMode(13, OUTPUT);

}

void loop() {
   
// put your main code here, to run repeatedly:
analogWrite(13, brightness);

  // change the brightness for next time through the loop:
  brightness = brightness + fadeAmount;

  // reverse the direction of the fading at the ends of the fade:
  if (brightness <= 0 || brightness >= 255) {
    fadeAmount = -fadeAmount;
  }
  // wait for 30 milliseconds to see the dimming effect
  delay(30);
}
