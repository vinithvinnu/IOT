#include "DHT.h"
const byte DHTPIN=D2;  // NodeMcu Pin 2


#define DHTTYPE DHT11   // DHT 11


DHT dht(DHTPIN, DHTTYPE);

void setup() {
  Serial.begin(115200);
  Serial.println(F("DHT Sensor Connect to Nodmcu"));
  dht.begin();
}

void loop() { 
  delay(2000);
  float h = dht.readHumidity();  
  float t = dht.readTemperature(); 
  Serial.print(F("Humidity: "));
  Serial.print(h);
  Serial.print(" %\t");
  Serial.print(F("Temperature: "));
  Serial.print(t);
  Serial.print(" *C ");
  Serial.println();
}
