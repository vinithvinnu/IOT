#include "DHT.h"
const byte DHTPIN=D7;
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);
void setup() {
  // put your setup code here, to run once:
Serial.begin(115200);

Serial.println(F("DHT sensor connect"));
dht.begin();

}
void loop() {
  // put your main code here, to run repeatedly:
delay(2000);
float h = dht.readHumidity();
float t = dht.readTemperature();
Serial.print(F("Humidity:"));
Serial.print(h);
Serial.print("%t");
Serial.print(F("Temperature"));
Serial.print(t);
Serial.print(" *C ");
Serial.println();

 }
